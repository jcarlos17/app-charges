@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('entries') }}">Gestión de ingreso por lotes</a></li>
            <li class="breadcrumb-item active">Nuevo ingreso</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Nuevo cobro</div>

        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success" role="alert">
                    {{ session('notification') }}
                </div>
            @endif
            <form method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Tasa</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                        </div>
                        @error('name')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="file" class="col-form-label">Archivo .csv</label>
                            <div class="custom-file">
                                <label class="custom-file-label" for="file">Seleccionar Archivo</label>
                                <input type="file" class="custom-file-input" id="file" name="file"  accept=".csv" required>
                            </div>
                            @error('file')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <a href="{{ asset('entries') }}" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')

@endsection