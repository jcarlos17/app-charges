@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Gestión de ingreso por lotes</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Gestión de ingreso por lotes</div>

        <div class="card-body">
            @include('includes.alerts')
            <div class="row">
                <div class="col-sm-8">
                    {{--<form>--}}
                        {{--<div class="form-row align-items-center">--}}
                            {{--<div class="col-auto">--}}
                                {{--<input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Buscar">--}}
                            {{--</div>--}}
                            {{--<div class="col-auto">--}}
                                {{--<button type="submit" class="btn btn-dark mb-2">Buscar</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>
                <div class="col-sm-4">
                    <a href="{{ asset('entries/create') }}" class="btn btn-dark float-right">Nuevo ingreso</a>
                </div>
            </div>
                <table class="table table-sm my-3">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Tasa</th>
                            <th>Total</th>
                            <th>Registros</th>
                            <th>F emisión</th>
                            <th>Usuario</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($entries as $entry)
                            <tr>
                                <td>{{ $entry->id }}</td>
                                <td>{{ $entry->name }}</td>
                                <td>{{ $entry->total }}</td>
                                <td>{{ $entry->details->count() }}</td>
                                <td>{{ $entry->created_format }}</td>
                                <td>{{ $entry->user->name }}</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-primary">Ingresar</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
@endsection
