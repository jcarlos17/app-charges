@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('accounts') }}">Cuenta</a></li>
            <li class="breadcrumb-item active">Editar cuenta</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Editar cuenta</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="post">
                @csrf
                <div class="form-group row">
                    <label for="client_id" class="col-sm-2 col-form-label">Cliente</label>
                    <div class="col-sm-6">
                        <select name="client_id" id="client_id" class="form-control @error('client_id') is-invalid @enderror" required>
                            <option value="">Seleccionar cliente</option>
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}" {{ old('client_id', $account->client_id) == $client->id ? 'selected' : '' }}>{{ $client->name }}</option>
                            @endforeach
                        </select>
                        @error('client_id')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="rate_id" class="col-sm-2 col-form-label">Tasa</label>
                    <div class="col-sm-6">
                        <select name="rate_id" id="rate_id" class="form-control @error('rate_id') is-invalid @enderror" required>
                            <option value="">Seleccionar tasa</option>
                            @foreach($rates as $rate)
                                <option value="{{ $rate->id }}" {{ old('rate_id', $account->rate_id) == $rate->id ? 'selected' : '' }}>{{ $rate->name }}</option>
                            @endforeach
                        </select>
                        @error('rate_id')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Código</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control @error('code') is-invalid @enderror" id="code"
                               name="code" value="{{ old('code', $account->code) }}" required>
                        @error('code')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mt-5">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{ asset('accounts') }}" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
