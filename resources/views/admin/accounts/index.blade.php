@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Cuentas</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Cuentas</div>

        <div class="card-body">
            @include('includes.alerts')
            <account-component></account-component>
        </div>
    </div>
@endsection
