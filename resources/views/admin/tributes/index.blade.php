@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Tributos</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Tributos</div>

        <div class="card-body">
            @include('includes.alerts')
            <div class="row">
                <div class="col-sm-8">
                    {{--<form>--}}
                        {{--<div class="form-row align-items-center">--}}
                            {{--<div class="col-auto">--}}
                                {{--<input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Buscar">--}}
                            {{--</div>--}}
                            {{--<div class="col-auto">--}}
                                {{--<button type="submit" class="btn btn-dark mb-2">Buscar</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>
                <div class="col-sm-4">
                    <a href="{{ asset('tributes/create') }}" class="btn btn-dark float-right">Nuevo tributo</a>
                </div>
            </div>
                <table class="table table-sm my-3">
                    <thead class="thead-light">
                        <tr>
                            <th>ID</th>
                            <th>Cliente</th>
                            <th>Tasa</th>
                            <th>Total</th>
                            <th>Fecha Pago</th>
                            <th>Fecha Emisión</th>
                            <th>Usuario</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tributes as $tribute)
                        <tr>
                            <td>{{ $tribute->id }}</td>
                            <td>{{ $tribute->client->name }}</td>
                            <td>{{ $tribute->rate->name }}</td>
                            <td>{{ $tribute->total }}</td>
                            <td></td>
                            <td>{{ $tribute->issue_date }}</td>
                            <td>{{ $tribute->user->name }}</td>
                            <td>
                                <a href="{{ url('tributes/'.$tribute->id.'/show') }}" class="btn btn-sm btn-success">Ver</a>
                                <a href="{{ url('tributes/'.$tribute->id.'/edit') }}" class="btn btn-sm btn-primary">E</a>
                                <a href="{{ url('tributes/'.$tribute->id.'/delete') }}" class="btn btn-sm btn-danger"
                                   onclick="return confirm('¿Estás seguro que deseas eliminar?');">B</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    </div>
@endsection
