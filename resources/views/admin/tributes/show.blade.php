@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('tributes') }}">Tributo</a></li>
            <li class="breadcrumb-item active">Ver tributo</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Ver tributo</div>

        <div class="card-body">
            <div class="form-group row">
                <label for="client_id" class="col-sm-2 col-form-label">Cliente</label>
                <div class="col-sm-6">
                    <select name="client_id" id="client_id" class="form-control" disabled>
                        <option selected>{{ $tribute->client->name }}</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="rate_id" class="col-sm-2 col-form-label">Tasa</label>
                <div class="col-sm-6">
                    <select name="rate_id" id="rate_id" class="form-control" disabled>
                        <option selected>{{ $tribute->rate->name }}</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="issue_date" class="col-sm-2 col-form-label">Fecha emisión</label>
                <div class="col-sm-3">
                    <input type="date" class="form-control" id="issue_date" name="issue_date" value="{{ $tribute->issue_date }}" disabled>
                </div>
            </div>
            <h4><span class="d-block py-2 badge badge-dark">Impuestos</span></h4>
            <div class="table-responsive" id="table">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th class="w-75">Impuesto</th>
                        <th class="w-25 text-center">Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tribute->details as $detail)
                        <tr>
                            <th scope="row">{{ $detail->tax_id }}</th>
                            <td class="w-50">{{ $detail->tax->name }}</td>
                            <td class="text-center w-25">
                                <input type="hidden" value="{{ $detail->tax_id }}">
                                <input type="number" step="0.01" class="form form-control form-control-sm"required value="{{ $detail->value }}" disabled>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="2" class="text-right">Total</th>
                        <td class="text-center w-25">
                            <div class="form-group">
                                <input type="number" class="form form-control form-control-sm" value="{{ $tribute->total }}" disabled>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group row mt-3">
                <div class="col-sm-10">
                    <a href="{{ asset('tributes') }}" class="btn btn-secondary">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection