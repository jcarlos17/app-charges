@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('tributes') }}">Tributo</a></li>
            <li class="breadcrumb-item active">Nuevo tributo</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Nuevo tributo</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="post">
                @csrf
                <div class="form-group row">
                    <label for="client_id" class="col-sm-2 col-form-label">Cliente</label>
                    <div class="col-sm-6">
                        <select name="client_id" id="client_id" class="form-control @error('client_id') is-invalid @enderror" required>
                            <option value="">Seleccionar cliente</option>
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : '' }}>{{ $client->name }}</option>
                            @endforeach
                        </select>
                        @error('client_id')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="rate_id" class="col-sm-2 col-form-label">Tasa</label>
                    <div class="col-sm-6">
                        <select name="rate_id" id="rate_id" class="form-control @error('rate_id') is-invalid @enderror" required>
                            <option value="">Seleccionar tasa</option>
                        </select>
                        @error('rate_id')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="issue_date" class="col-sm-2 col-form-label">Fecha emisión</label>
                    <div class="col-sm-3">
                        <input type="date" class="form-control @error('issue_date') is-invalid @enderror" id="issue_date"
                               name="issue_date" value="{{ old('issue_date') }}" required>
                        @error('issue_date')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <h4><span class="d-block py-2 badge badge-dark">Impuestos</span></h4>
                <div class="table-responsive" id="table">
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <a href="{{ asset('tributes') }}" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('js/tribute.js') }}"></script>
@endsection