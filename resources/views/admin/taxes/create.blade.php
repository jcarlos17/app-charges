@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('rates') }}">Tasas</a></li>
            <li class="breadcrumb-item"><a href="{{ asset('rates/'.$rate->id.'/taxes') }}">{{ $rate->name }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo impuesto</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">
            Nuevo impuesto
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="post">
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Impuesto</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                               name="name" value="{{ old('name') }}" required>
                        @error('name')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="account" class="col-sm-2 col-form-label">Cuenta</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control @error('account') is-invalid @enderror" id="account"
                               name="account" value="{{ old('account') }}" required>
                        @error('account')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mt-5">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <a href="{{ asset('rates/'.$rate->id.'/taxes') }}" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
