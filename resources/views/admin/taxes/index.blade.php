@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('rates') }}">Tasas</a></li>
            <li class="breadcrumb-item">{{ $rate->name }}</li>
            <li class="breadcrumb-item active">Impuestos</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">
            Impuestos
        </div>

        <div class="card-body">
            @include('includes.alerts')
            <tax-component></tax-component>

            <a href="{{ url('rates') }}" class="btn btn-secondary mt-3">Regresar a tasas</a>
        </div>
    </div>
@endsection
