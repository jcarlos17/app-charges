@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Tasas</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Listado de tasas</div>

        <div class="card-body">
            @include('includes.alerts')
            <rate-component></rate-component>
        </div>
    </div>
@endsection
