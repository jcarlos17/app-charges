@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('payments') }}">Cobros</a></li>
            <li class="breadcrumb-item active">Nuevo cobro</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Nuevo cobro</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="post">
                @csrf
                <div class="form-group row">
                    <label for="client_id" class="col-sm-2 col-form-label">Cliente</label>
                    <div class="col-sm-6">
                        <select name="client_id" id="client_id" class="form-control @error('client_id') is-invalid @enderror">
                            <option value="">Buscar cliente</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="identification_card" class="col-form-label">Cedula</label>
                            <input type="text" class="form-control" id="identification_card" value="1800000009">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="address" class="col-form-label">Dirección</label>
                            <input type="text" class="form-control" id="address" value="Direccion cliente nn">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="payment_date" class="col-form-label">Fecha cobro</label>
                            <input type="date" class="form-control @error('payment_date') is-invalid @enderror" id="payment_date"
                                   name="payment_date" value="2020-04-22">
                            @error('payment_date')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <label for="rate_id" class="col-sm-2 col-form-label">Código tasa</label>
                    <div class="col-sm-3">
                        <select name="rate_id" id="rate_id" class="form-control @error('rate_id') is-invalid @enderror">
                            <option value="">Buscar tasa</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Tasa</label>
                            <input type="text" class="form-control" id="name" value="Mejoras">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="code" class="col-form-label">#</label>
                            <input type="text" class="form-control" id="code" value="2">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="total" class="col-form-label">Total</label>
                            <input type="text" class="form-control" id="total" value="30.20">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="from_date" class="col-form-label">Desde</label>
                            <input type="month" class="form-control @error('from_date') is-invalid @enderror" id="from_date"
                                   name="from_date" value="2020-01">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="to_date" class="col-form-label">Hasta</label>
                            <input type="month" class="form-control @error('to_date') is-invalid @enderror" id="to_date"
                                   name="to_date" value="2020-02">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-dark">Agregar</button>
                </div>
                <div class="table-responsive" id="table">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th class="w-75" scope="col">Impuesto</th>
                            <th class="w-25 text-center" scope="col">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Valor mejoras</td>
                                <td class="text-right">28.20</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Interes</td>
                                <td class="text-right">2.00</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Coactiva</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th scope="row">4</th>
                                <td>Juicio</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th scope="row">5</th>
                                <td>Carpeta</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th class="text-right" colspan="2">Total</th>
                                <td class="text-right">30.20</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-10">
                        <button type="button" class="btn btn-primary">Aceptar</button>
                        <a href="{{ asset('payments') }}" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')

@endsection