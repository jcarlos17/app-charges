@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Cobros</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Cobros</div>

        <div class="card-body">
            @include('includes.alerts')
            <div class="row">
                <div class="col-sm-8">
                    {{--<form>--}}
                        {{--<div class="form-row align-items-center">--}}
                            {{--<div class="col-auto">--}}
                                {{--<input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Buscar">--}}
                            {{--</div>--}}
                            {{--<div class="col-auto">--}}
                                {{--<button type="submit" class="btn btn-dark mb-2">Buscar</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>
                <div class="col-sm-4">
                    <a href="{{ asset('payments/create') }}" class="btn btn-dark float-right">Nuevo cobro</a>
                </div>
            </div>
                <table class="table table-sm my-3">
                    <thead class="thead-light">
                        <tr>
                            <th>ID</th>
                            <th># Cobro</th>
                            <th>Cliente</th>
                            <th>Tasa</th>
                            <th>Total</th>
                            <th>Fecha Pago</th>
                            <th>Usuario</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>1</td>
                            <td>Nombre apellido 1</td>
                            <td>Regalias</td>
                            <td>5.20</td>
                            <td>11-01-2019</td>
                            <td>EArcos</td>
                            <td>
                                <a href="#" class="btn btn-sm btn-success">Ver</a>
                                <a href="#" class="btn btn-sm btn-primary">E</a>
                                <a href="#" class="btn btn-sm btn-danger"
                                   onclick="return confirm('¿Estás seguro que deseas eliminar?');">B</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </div>
@endsection
