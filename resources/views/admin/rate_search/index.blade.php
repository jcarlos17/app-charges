@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Buscar tasas</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Buscar tasas</div>

        <div class="card-body">
            @include('includes.alerts')
            <div class="row">
                <div class="col-sm-8">
                    {{--<form>--}}
                        {{--<div class="form-row align-items-center">--}}
                            {{--<div class="col-auto">--}}
                                {{--<input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Buscar">--}}
                            {{--</div>--}}
                            {{--<div class="col-auto">--}}
                                {{--<button type="submit" class="btn btn-dark mb-2">Buscar</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-sm my-3">
                    <thead class="thead-light">
                    <tr>
                        <th></th>
                        <th>Id Cuenta</th>
                        <th>Tasa</th>
                        <th class="w-25">Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <button class="btn btn-sm btn-primary">+</button>
                        </td>
                        <td>1</td>
                        <td>Regalias</td>
                        <td>5.20</td>
                        <td>
                            <a href="#" class="btn btn-sm btn-primary">Pasar a cobros</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-sm btn-primary">-</button>
                        </td>
                        <td>2</td>
                        <td>Regalias</td>
                        <td>5.20</td>
                        <td>
                            <a href="#" class="btn btn-sm btn-primary">Pasar a cobros</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table class="table table-sm table-bordered">
                                <thead>
                                <tr>
                                    <th>ID Título</th>
                                    <th>Mes</th>
                                    <th>Año</th>
                                    <th>valor</th>
                                    <th>Interes</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Enero</td>
                                    <td>2020</td>
                                    <td>9.20</td>
                                    <td>1.00</td>
                                    <td>10.20</td>
                                    <td>
                                        <input type="checkbox" id="a1" checked>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Febrero</td>
                                    <td>2020</td>
                                    <td>9.20</td>
                                    <td>1.00</td>
                                    <td>10.20</td>
                                    <td>
                                        <input type="checkbox" id="a2" checked>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Marzo</td>
                                    <td>2020</td>
                                    <td>9.20</td>
                                    <td>1.00</td>
                                    <td>10.20</td>
                                    <td>
                                        <input type="checkbox" id="a3">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-sm btn-primary">+</button>
                        </td>
                        <td>3</td>
                        <td>Regalias</td>
                        <td>5.20</td>
                        <td>
                            <a href="#" class="btn btn-sm btn-primary">Pasar a cobros</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <a href="{{ asset('#') }}" class="btn btn-secondary">Regresar</a>
        </div>
    </div>
@endsection
