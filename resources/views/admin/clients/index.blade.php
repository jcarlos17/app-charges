@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Clientes</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Clientes</div>

        <div class="card-body">
            @include('includes.alerts')
            <client-component></client-component>
        </div>
    </div>
@endsection
