@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('clients') }}">Clientes</a></li>
            <li class="breadcrumb-item">{{ $client->name }}</li>
            <li class="breadcrumb-item active">Editar cliente</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header text-white bg-dark">Editar cliente</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="post">
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nombres</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                               name="name" value="{{ old('name', $client->name) }}" required>
                        @error('name')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="identification_card" class="col-sm-2 col-form-label">Cédula</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control @error('identification_card') is-invalid @enderror" required
                               id="identification_card" name="identification_card" value="{{ old('identification_card', $client->identification_card) }}">
                        @error('identification_card')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">Dirección</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control @error('address') is-invalid @enderror" id="address"
                               name="address" value="{{ old('address', $client->address) }}" required>
                        @error('address')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Correo</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                               name="email" value="{{ old('email', $client->email) }}" required>
                        @error('email')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-sm-2 col-form-label">Telefono</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone"
                               name="phone" value="{{ old('phone', $client->phone) }}" required>
                        @error('phone')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>

                </div>
                <div class="form-group row mb-5">
                    <label for="cellphone" class="col-sm-2 col-form-label">Celular</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control @error('cellphone') is-invalid @enderror" id="cellphone"
                               name="cellphone" value="{{ old('cellphone', $client->cellphone) }}" required>
                        @error('cellphone')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{ asset('clients') }}" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
