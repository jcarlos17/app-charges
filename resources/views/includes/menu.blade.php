@if(auth()->check())
    <div class="text-center">
        <img src="{{ asset('images/users/0.jpg') }}" title="{{ auth()->user()->name }}" class="img-circle img-responsive">
    </div>
@endif
<div class="card">
    <div class="card-header text-white bg-dark">Menú</div>

    <div class="card-body">
        <div class="list-group">
            @guest
                <a href="{{ url('/') }}" class="list-group-item list-group-item-action {{ request()->is('/') ? 'active' : '' }}">
                    Bienvenido
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    Instrucciones
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    Créditos
                </a>
            @else
                <a href="{{ url('home') }}" class="list-group-item list-group-item-action {{ request()->is('home') ? 'active' : '' }}">
                    Tablero
                </a>
                <a href="{{ url('clients') }}" class="list-group-item list-group-item-action {{ request()->is('clients*') ? 'active' : '' }}">
                    Clientes
                </a>
                <a href="{{ url('rates') }}" class="list-group-item list-group-item-action {{ request()->is('rates*') || request()->is('taxes*') ? 'active' : '' }}">
                    Tasas
                </a>
                <a href="{{ url('accounts') }}" class="list-group-item list-group-item-action {{ request()->is('accounts*') ? 'active' : '' }}">
                    Cuentas
                </a>

                <a href="{{ url('tributes') }}" class="list-group-item list-group-item-action {{ request()->is('tributes*') ? 'active' : '' }}">
                    Tributos
                </a>

                <a href="{{ url('entries') }}" class="list-group-item list-group-item-action {{ request()->is('entries*') ? 'active' : '' }}">
                    Tributo por Lotes
                </a>

                <a href="{{ url('payments') }}" class="list-group-item list-group-item-action {{ request()->is('payments*') ? 'active' : '' }}">
                    Cobros
                </a>
                <a href="{{ url('rate/search') }}" class="list-group-item list-group-item-action {{ request()->is('rate/search*') ? 'active' : '' }}">
                    Buscar tasas
                </a>
            @endguest
        </div>
    </div>
</div>