@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header text-white bg-dark">Tablero</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            Bienvenido {{ auth()->user()->name }}
        </div>
    </div>
@endsection
