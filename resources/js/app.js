require('./bootstrap');

window.Vue = require('vue');

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.component('client-component', require('./components/ClientComponent.vue').default);
Vue.component('rate-component', require('./components/RateComponent.vue').default);
Vue.component('account-component', require('./components/AccountComponent.vue').default);
Vue.component('tax-component', require('./components/TaxComponent.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));

const app = new Vue({
    el: '#app',
});
