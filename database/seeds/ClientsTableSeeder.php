<?php

use App\Client;
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $faker = Faker::create();
            foreach (range(1,10) as $index) {
                Client::create([
                    'name' => $faker->firstName,
                    'identification_card' => '18000000000'.$index,
                    'address' => $faker->address,
                    'email' => $faker->safeEmail,
                    'phone' => $faker->phoneNumber,
                    'cellphone' => $faker->phoneNumber
                ]);
            }
        }
        catch (\Illuminate\Database\QueryException $e) {
            $this->command->error("SQL Error: " . $e->getMessage() . "\n");
        }
    }
}
