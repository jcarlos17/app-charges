<?php

use App\Rate;
use App\Tax;
use Illuminate\Database\Seeder;

class RatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rate1 = Rate::create(['name' => 'Regalias']);
        $rate2 = Rate::create(['name' => 'Solicitud']);
        $rate3 = Rate::create(['name' => 'Línea de fabrica']);

        Tax::create(['name' => 'Valor mejora', 'account' => '1.1.10', 'rate_id' => $rate1->id]);
        Tax::create(['name' => 'Interes', 'account' => '1.1.11', 'rate_id' => $rate1->id]);
        Tax::create(['name' => 'Coactiva', 'account' => '1.1.12', 'rate_id' => $rate1->id]);
        Tax::create(['name' => 'Juicio', 'account' => '1.1.13', 'rate_id' => $rate1->id]);
        Tax::create(['name' => 'Carpeta', 'account' => '1.1.14', 'rate_id' => $rate1->id]);

        Tax::create(['name' => 'Valor mejora', 'account' => '1.1.15', 'rate_id' => $rate2->id]);
        Tax::create(['name' => 'Interes', 'account' => '1.1.16', 'rate_id' => $rate2->id]);
        Tax::create(['name' => 'Coactiva', 'account' => '1.1.17', 'rate_id' => $rate2->id]);
        Tax::create(['name' => 'Juicio', 'account' => '1.1.18', 'rate_id' => $rate2->id]);
        Tax::create(['name' => 'Carpeta', 'account' => '1.1.19', 'rate_id' => $rate2->id]);

        Tax::create(['name' => 'Valor mejora', 'account' => '1.1.20', 'rate_id' => $rate3->id]);
        Tax::create(['name' => 'Interes', 'account' => '1.1.21', 'rate_id' => $rate3->id]);
        Tax::create(['name' => 'Coactiva', 'account' => '1.1.22', 'rate_id' => $rate3->id]);
        Tax::create(['name' => 'Juicio', 'account' => '1.1.23', 'rate_id' => $rate3->id]);
        Tax::create(['name' => 'Carpeta', 'account' => '1.1.24', 'rate_id' => $rate3->id]);
    }
}
