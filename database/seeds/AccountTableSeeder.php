<?php

use App\Account;
use Illuminate\Database\Seeder;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::create(['client_id' => 1, 'rate_id' => 1, 'code' => 1]);
        Account::create(['client_id' => 2, 'rate_id' => 2, 'code' => 2]);
        Account::create(['client_id' => 3, 'rate_id' => 3, 'code' => 3]);
    }
}
