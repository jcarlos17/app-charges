<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTributeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tribute_details', function (Blueprint $table) {
            $table->increments('id');

            $table->float('value');

            $table->integer('tax_id')->unsigned();
            $table->foreign('tax_id')->references('id')->on('taxes');

            $table->integer('tribute_id')->unsigned();
            $table->foreign('tribute_id')->references('id')->on('tributes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tribute_details');
    }
}
