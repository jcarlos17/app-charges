<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth','admin'], 'namespace' => 'Admin'], function (){
    //clients
    Route::get('clients', 'ClientController@index');
    Route::get('clients/create', 'ClientController@create');
    Route::post('clients/create', 'ClientController@store');
    Route::get('clients/{id}/edit', 'ClientController@edit');
    Route::post('clients/{id}/edit', 'ClientController@update');
    Route::get('clients/{id}/delete', 'ClientController@delete');
    //rates
    Route::get('rates', 'RateController@index');
    Route::get('rates/create', 'RateController@create');
    Route::post('rates/create', 'RateController@store');
    Route::get('rates/{id}/edit', 'RateController@edit');
    Route::post('rates/{id}/edit', 'RateController@update');
    Route::get('rates/{id}/delete', 'RateController@delete');
    //taxes
    Route::get('rates/{id}/taxes', 'TaxController@index');
    Route::get('rates/{id}/taxes/create', 'TaxController@create');
    Route::post('rates/{id}/taxes/create', 'TaxController@store');
    Route::get('taxes/{tax_id}/edit', 'TaxController@edit');
    Route::post('taxes/{tax_id}/edit', 'TaxController@update');
    Route::get('taxes/{tax_id}/delete', 'TaxController@delete');
    //accounts
    Route::get('accounts', 'AccountController@index');
    Route::get('accounts/create', 'AccountController@create');
    Route::post('accounts/create', 'AccountController@store');
    Route::get('accounts/{id}/edit', 'AccountController@edit');
    Route::post('accounts/{id}/edit', 'AccountController@update');
    Route::get('accounts/{id}/delete', 'AccountController@delete');

    //tributes
    Route::get('tributes', 'TributeController@index');
    Route::get('tributes/create', 'TributeController@create');
    Route::post('tributes/create', 'TributeController@store');
    Route::get('tributes/{id}/show', 'TributeController@show');
    Route::get('tributes/{id}/edit', 'TributeController@edit');
    Route::post('tributes/{id}/edit', 'TributeController@update');
    Route::get('tributes/{id}/delete', 'TributeController@delete');

    //batch tributes
    Route::get('entries', 'EntryController@index');
    Route::get('entries/create', 'EntryController@create');
    Route::post('entries/create', 'EntryController@store');

    //payment
    Route::get('payments', 'PaymentController@index');
    Route::get('payments/create', 'PaymentController@create');
    Route::post('payments/create', 'PaymentController@store');
    Route::get('payments/{id}/show', 'PaymentController@show');
    Route::get('payments/{id}/edit', 'PaymentController@edit');
    Route::post('payments/{id}/edit', 'PaymentController@update');
    Route::get('payments/{id}/delete', 'PaymentController@delete');

    // Search
    Route::get('rate/search', 'RateSearchController@index');
});
