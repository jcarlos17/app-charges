<?php

Route::get('rates/{clientId}', 'RateController@byClient');
Route::get('taxes/{rateId}', 'TaxController@byRate');

//clients
Route::get('clients', 'SearchController@clients');
Route::get('rates', 'SearchController@rates');
Route::get('accounts', 'SearchController@accounts');
Route::get('rates/{id}/taxes', 'SearchController@taxes');
