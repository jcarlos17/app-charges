<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $fillable = ['name', 'account'];

    public static $rules = [
        'name' => 'required|string|max:255',
        'account' => 'required|string|max:255'
    ];

    public static $messages = [
        'name.required' => 'Es necesario este campo',
        'name.max' => 'Se acepta un máximo de 255 caracteres.',
        'account.required' => 'Es necesario este campo',
        'account.max' => 'Se acepta un máximo de 255 caracteres.'
    ];

    public function rate()
    {
        return $this->belongsTo(Rate::class);
    }
}
