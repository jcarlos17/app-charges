<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ['name'];

    public static $rules = [
        'name' => 'required|string|max:255'
    ];

    public static $messages = [
        'name.required' => 'Es necesario este campo',
        'name.max' => 'Se acepta un máximo de 255 caracteres.'
    ];

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function taxes()
    {
        return $this->hasMany(Tax::class);
    }
}
