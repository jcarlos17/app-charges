<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['client_id', 'rate_id', 'code'];

    protected $with = ['client', 'rate'];

    public static $rules = [
        'client_id' => 'required',
        'rate_id' => 'required',
        'code' => 'required|numeric',
    ];

    public static $messages = [
        'client_id.required' => 'Es necesario este campo',
        'rate_id.required' => 'Es necesario este campo',
        'code.required' => 'Es necesario este campo.',
        'code.numeric' => 'Ingrese un formato numérico.',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function rate()
    {
        return $this->belongsTo(Rate::class);
    }
}
