<?php

namespace App\Http\Controllers\Api;

use App\Rate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaxController extends Controller
{
    public function byRate($id)
    {
        $rate = Rate::findOrFail($id);

        return $rate->taxes;
    }
}
