<?php

namespace App\Http\Controllers\Api;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RateController extends Controller
{
    public function byClient($id)
    {
        $client = Client::findOrFail($id);

        return $client->rates;
    }
}
