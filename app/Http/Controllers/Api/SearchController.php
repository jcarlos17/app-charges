<?php

namespace App\Http\Controllers\Api;

use App\Account;
use App\Client;
use App\Rate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    public function clients(Request $request)
    {
        $nameInput = $request->nameInput;
        $query = Client::query();

        if ($nameInput) {
            $query = $query->where('name', 'like', "%$nameInput%");
        }

        return $query->paginate(10);
    }

    public function rates(Request $request)
    {
        $nameInput = $request->nameInput;
        $query = Rate::query();

        if ($nameInput) {
            $query = $query->where('name', 'like', "%$nameInput%");
        }

        $rates = $query->paginate(10);
        return $rates;
    }

    public function accounts(Request $request)
    {
        $nameInput = $request->nameInput;
        $query = Account::with('client');

        if ($nameInput) {
            $query = $query->whereHas('client', function($queryClient) use($nameInput) {
                $queryClient->where('name', 'like', "%$nameInput%");
            });
        }

        $accounts = $query->paginate(10);
        return $accounts;
    }

    public function taxes($rateId, Request $request)
    {
        $nameInput = $request->nameInput;

        $rate = Rate::findOrFail($rateId);
        $query = $rate->taxes();

        if ($nameInput) {
            $query = $query->where('name', 'like', "%$nameInput%");
        }

        $taxes = $query->paginate(10);
        return $taxes;
    }

}
