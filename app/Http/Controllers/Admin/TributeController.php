<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\Client;
use App\Tribute;
use App\TributeDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TributeController extends Controller
{
    public function index()
    {
        $tributes = Tribute::paginate(15);

        return view('admin.tributes.index', compact('tributes'));
    }

    public function create()
    {
        $clientIds = Account::groupBy('client_id')->pluck('client_id');
        $clients = Client::whereIn('id', $clientIds)->get();

        return view('admin.tributes.create', compact('clients'));
    }

    public function store(Request $request)
    {
        $tribute = new Tribute();
        $tribute->issue_date = $request->issue_date;
        $tribute->rate_id = $request->rate_id;
        $tribute->client_id = $request->client_id;
        $tribute->user_id = auth()->id();
        $tribute->save();

        $taxIds = $request->tax_ids;
        $values = $request->values;

        foreach ($taxIds as $key => $taxId) {
            $detail = new TributeDetail();
            $detail->tax_id = $taxId;
            $detail->value = $values[$key];
            $detail->tribute_id = $tribute->id;
            $detail->save();
        }

        return redirect('tributes')->with('notification', 'Se registró correctamente');
    }

    public function edit($id)
    {
        $tribute = Tribute::findOrFail($id);
        $clientIds = Account::groupBy('client_id')->pluck('client_id');
        $clients = Client::whereIn('id', $clientIds)->get();

        return view('admin.tributes.edit', compact('clients', 'tribute'));
    }

    public function update(Request $request, $id)
    {
        $tribute = Tribute::findOrFail($id);
        $tribute->issue_date = $request->issue_date;
        $tribute->rate_id = $request->rate_id;
        $tribute->client_id = $request->client_id;
        $tribute->save();

        $taxIds = $request->tax_ids;
        $values = $request->values;

        $tribute->details()->delete();

        foreach ($taxIds as $key => $taxId) {
            $detail = new TributeDetail();
            $detail->tax_id = $taxId;
            $detail->value = $values[$key];
            $detail->tribute_id = $tribute->id;
            $detail->save();
        }

        return redirect('tributes')->with('notification', 'Se guardaron los cambios correctamente');
    }

    public function show($id)
    {
        $tribute = Tribute::findOrFail($id);
        $clientIds = Account::groupBy('client_id')->pluck('client_id');
        $clients = Client::whereIn('id', $clientIds)->get();

        return view('admin.tributes.show', compact('clients', 'tribute'));
    }

    public function delete($id)
    {
        Tribute::findOrFail($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
