<?php

namespace App\Http\Controllers\Admin;

use App\Rate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RateController extends Controller
{
    public function index()
    {
        $rates = Rate::paginate(10);

        return view('admin.rates.index', compact('rates'));
    }

    public function create()
    {
        return view('admin.rates.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Rate::$rules, Rate::$messages);

        $data = $request->except('_token');
        Rate::create($data);

        return redirect('rates')->with('notification', 'Se registró correctamente.');
    }

    public function edit($id)
    {
        $rate = Rate::findOrFail($id);

        return view('admin.rates.edit', compact('rate'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Rate::$rules, Rate::$messages);

        $data = $request->except('_token');
        $rate = Rate::findOrFail($id);
        $rate->update($data);

        return redirect('rates')->with('notification', 'Se guardaron los cambios correctamente.');
    }

    public function delete($id)
    {
        $rate = Rate::findOrFail($id);

        if ($rate->accounts()->exists() || $rate->taxes()->exists())
            return back()->with('error', 'No se puede eliminar esta tasa.');

        $rate->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
