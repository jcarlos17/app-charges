<?php

namespace App\Http\Controllers\Admin;

use App\Entry;
use App\EntryDetail;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class EntryController extends Controller
{
    public function index()
    {
        $entries = Entry::orderBy('created_at')->get();

        return view('admin.entries.index', compact('entries'));
    }

    public function create()
    {
        return view('admin.entries.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'file' => 'required|mimes:csv,txt'
        ];
        $messages = [
            'file.mimes' => 'Los datos se deben cargar a través de un archivo CSV.'
        ];
        $this->validate($request, $rules, $messages);

        try {
            DB::beginTransaction();
            $file = $request->file('file');
            $lines = file($file);
            $utf8_lines = array_map('utf8_encode', $lines);
            $array = array_map(function($v){return str_getcsv($v, "\t");}, $utf8_lines);

            $entry = new Entry();
            $entry->name = $request->name;
            $entry->user_id = auth()->id();
            $entry->save();

            for ($i=0; $i<sizeof($array); ++$i) {
                $col1 = $array[$i][0];
                $col2 = $array[$i][1];
                $col3 = $array[$i][2];
                $col4 = $array[$i][3];
                $col5 = $array[$i][4];
                $col6 = $array[$i][5];
                $col7 = $array[$i][6];
                $col8 = $array[$i][7];
                $col9 = $array[$i][8];
                $col10 = $array[$i][9];
                $col11 = $array[$i][10];
                $col12 = $array[$i][11];
                $col13 = $array[$i][12];

                $entryDetail = new EntryDetail();
                $entryDetail->col_1 = $col1;
                $entryDetail->col_2 = $col2;
                $entryDetail->col_3 = $col3;
                $entryDetail->col_4 = $col4;
                $entryDetail->col_5 = $col5;
                $entryDetail->col_6 = $col6;
                $entryDetail->col_7 = $col7;
                $entryDetail->col_8 = $col8;
                $entryDetail->col_9 = $col9;
                $entryDetail->col_10 = $col10;
                $entryDetail->col_11 = $col11;
                $entryDetail->col_12 = $col12;
                $entryDetail->col_13 = $col13;
                $entryDetail->entry_id = $entry->id;
                $entryDetail->save();
            }
            DB::commit();
            return redirect('entries')->with('notification', 'Se registró correctamente.');

        } catch (Exception $e) {

            DB::rollback();
            return redirect('/entries/create')->with('notification', $e->getMessage());
//            return redirect('/entries/create')->with('notification', 'El archivo cargado no tiene un formato correcto.');
        }
    }
}
