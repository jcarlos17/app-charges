<?php

namespace App\Http\Controllers\Admin;

use App\Rate;
use App\Tax;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaxController extends Controller
{
    public function index($id)
    {
        $rate = Rate::findOrFail($id);
        $taxes = $rate->taxes()->paginate(10);

        return view('admin.taxes.index', compact('taxes', 'rate'));
    }

    public function create($id)
    {
        $rate = Rate::findOrFail($id);

        return view('admin.taxes.create', compact('rate'));
    }

    public function store(Request $request, $rateId)
    {
        $this->validate($request, Tax::$rules, Tax::$messages);

        $tax = new Tax();
        $tax->name = $request->name;
        $tax->account = $request->account;
        $tax->rate_id = $rateId;
        $tax->save();

        return redirect('rates/'.$rateId.'/taxes')->with('notification', 'Se registró correctamente.');
    }

    public function edit($taxId)
    {
        $tax = Tax::findOrFail($taxId);

        return view('admin.taxes.edit', compact('tax'));
    }

    public function update(Request $request, $taxId)
    {
        $this->validate($request, Tax::$rules, Tax::$messages);

        $data = $request->except('_token');
        $tax = Tax::findOrFail($taxId);
        $tax->update($data);

        return redirect('rates/'.$tax->rate_id.'/taxes')->with('notification', 'Se guardaron los cambios correctamente.');
    }

    public function delete($id)
    {
        Tax::findOrFail($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
