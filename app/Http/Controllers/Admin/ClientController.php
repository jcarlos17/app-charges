<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::paginate(10);

        return view('admin.clients.index', compact('clients'));
    }

    public function create()
    {
        return view('admin.clients.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Client::$rules, Client::$messages);

        $data = $request->except('_token');
        Client::create($data);

        return redirect('clients')->with('notification', 'Se registró correctamente.');
    }

    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.edit', compact('client'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Client::$rules, Client::$messages);

        $data = $request->except('_token');
        $client = Client::findOrFail($id);
        $client->update($data);

        return redirect('clients')->with('notification', 'Se guardaron los cambios correctamente.');
    }

    public function delete($id)
    {
        $client = Client::findOrFail($id);

        if ($client->accounts()->exists())
            return back()->with('error', 'No se puede eliminar este usuario.');

        $client->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
