<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\Client;
use App\Rate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function index()
    {
        $accounts = Account::paginate(10);

        return view('admin.accounts.index', compact('accounts'));
    }

    public function create()
    {
        $clients = Client::orderBy('name')->get();
        $rates = Rate::orderBy('name')->get();

        return view('admin.accounts.create', compact('clients', 'rates'));
    }

    public function store(Request $request)
    {
        $this->validate($request, Account::$rules, Account::$messages);

        $data = $request->except('_token');
        Account::create($data);

        return redirect('accounts')->with('notification', 'Se registró correctamente.');
    }

    public function edit($id)
    {
        $account = Account::findOrFail($id);

        $clients = Client::orderBy('name')->get();
        $rates = Rate::orderBy('name')->get();

        return view('admin.accounts.edit', compact('account', 'clients', 'rates'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Account::$rules, Account::$messages);

        $data = $request->except('_token');
        $account = Account::findOrFail($id);
        $account->update($data);

        return redirect('accounts')->with('notification', 'Se guardaron los cambios correctamente.');
    }

    public function delete($id)
    {
        Account::findOrFail($id)->delete();

        return back()->with('notification', 'Se eliminó correctamente.');
    }
}
