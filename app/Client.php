<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'identification_card', 'address', 'email', 'phone', 'cellphone'];

    protected $appends = ['id_str', 'updated_format'];

    public static $rules = [
        'name' => 'required|string|max:255',
        'identification_card' => 'required|numeric|digits:12',
        'address' => 'required|string|max:255',
        'email' => 'required|email|max:255',
        'phone' => 'required|numeric',
        'cellphone' => 'required|numeric'
    ];

    public static $messages = [
        'name.required' => 'Es necesario este campo',
        'name.max' => 'Se acepta un máximo de 255 caracteres.',
        'identification_card.required' => 'Es necesario este campo.',
        'identification_card.numeric' => 'Ingrese un formato numérico.',
        'identification_card.digits' => 'Ingrese un valor con 12 caracteres.',
        'address.required' => 'Es necesario este campo.',
        'address.max' => 'Se acepta un máximo de 255 caracteres.',
        'email.required' => 'Es necesrio este campo.',
        'email.email' => 'Ingrese un formato de correo',
        'email.max' => 'Se acepta un máximo de 255 caracteres.',
        'phone.required' => 'Es necesario este campo.',
        'phone.numeric' => 'Ingrese un formato numérico.',
        'cellphone.required' => 'Es necesario este campo.',
        'cellphone.numeric' => 'Ingrese un formato numérico.',

    ];

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function rates()
    {
        return $this->belongsToMany(Rate::class, 'accounts', 'client_id', 'rate_id');
    }

    public function getIdStrAttribute()
    {
        return str_pad($this->id, 5, "0", STR_PAD_LEFT);
    }

    public function getUpdatedFormatAttribute()
    {
        return $this->updated_at->format('d-m-Y');
    }
}
