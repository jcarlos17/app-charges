<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tribute extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function rate()
    {
        return $this->belongsTo(Rate::class);
    }

    public function details()
    {
        return $this->hasMany(TributeDetail::class);
    }

    public function getTotalAttribute()
    {
        return $this->details()->sum('value');
    }
}
