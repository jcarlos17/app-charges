<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function details()
    {
        return $this->hasMany(EntryDetail::class);
    }

    public function getCreatedFormatAttribute()
    {
        return $this->created_at->format('d-m-Y');
    }

    public function getTotalAttribute()
    {
        return $this->details()->sum('col_12');
    }
}
