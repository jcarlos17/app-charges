$(function() {
    const $selectClient = $('#client_id');
    const $selectRate = $('#rate_id');
    const $table = $('#table');

    $selectClient.on('change', onSelectRates);
    $selectRate.on('change', onShowTaxes);

    function onSelectRates() {
        let clientId = $selectClient.val();
        // AJAX
        $.get('/api/rates/'+clientId, function (rates) {
            let html = '<option value="">Seleccionar tasa</option>';
            $.each(rates, function(i, rate) {
                html += '<option value="'+rate.id+'">'+rate.name+'</option>';
            });

            $selectRate.html(html);
            $table.html('');
        });
    }

    function onShowTaxes() {
        let rateId = $selectRate.val();
        // AJAX
        $.get('/api/taxes/'+rateId, function (taxes) {
            $table.html('');

            let list = '';
            $.each(taxes, function(i, tax) {
                list += '<tr>'+
                    '<th scope="row">'+tax.id+'</th>'+
                    '<td class="w-50">'+tax.name+'</td>'+
                    '<td class="text-center w-25">'+
                    '<input type="hidden" name="tax_ids[]" value="'+tax.id+'">'+
                    '<input type="number" step="0.01" class="form form-control form-control-sm" data-value name="values[]" required value="0.00">'+
                    '</td>'+
                    '</tr>';
            });

            let tableHtml = '<table class="table">\n' +
                '<thead class="thead-light">\n' +
                '<tr>\n' +
                '<th>ID</th>\n' +
                '<th>Impuesto</th>\n' +
                '<th class="text-center">Valor</th>\n' +
                '</tr>\n' +
                '</thead>\n' +
                '<tbody>\n' +
                list +
                '<tr>\n' +
                '<th colspan="2" class="text-right">Total</th>\n' +
                '<td class="text-center w-25">\n' +
                '<div class="form-group">\n' +
                '<input type="number" class="form form-control form-control-sm" id="total" name="total" readonly>\n' +
                '</div>\n' +
                '</td>\n' +
                '</tr>\n' +
                '</tbody>\n' +
                '</table>';

            $table.html(tableHtml);

            let $values = $('[data-value]');

            $values.keyup(function() {
                let total = 0;
                $values.each(function(){
                    total += parseFloat($(this).val());
                });
                $('#total').val(total.toFixed(2));
            });
        });
    }
});